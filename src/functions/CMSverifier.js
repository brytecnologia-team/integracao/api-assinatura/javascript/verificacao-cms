const request = require('request');
const serviceConfig = require('../config/ServiceConfig');
const signatureConfig = require('../config/SignatureConfig');

var fs = require('fs');

const URL_SERVER_CMS = serviceConfig.URL_CMS_VERIFIER;

this.headers = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${serviceConfig.ACCESS_TOKEN}`
};

var auxAuthorization = this.headers.Authorization.split(' ');

const configuredToken = () => {
    if (auxAuthorization[1] === '<INSERT_VALID_ACCESS_TOKEN>') {
        console.log('Set up a valid token');
        return false;
    }
    return true;
}


const verifyCAdESattachedSignature = (URL_SERVER_CMS) => {

    const verificationForm = {
        'nonce': signatureConfig.NONCE,
        'signatures[0][nonce]': signatureConfig.NONCE_OF_SIGNATURE,
        'signatures[0][content]': fs.createReadStream(signatureConfig.ATTACHED_SIGNATURE_PATH)
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL_SERVER_CMS, formData: verificationForm, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}

const verifyCAdESdetachedSignature = (URL_SERVER_CMS) => {

    const verificationForm = {
        'nonce': signatureConfig.NONCE,
        'signatures[0][nonce]': signatureConfig.NONCE_OF_SIGNATURE,
        'signatures[0][content]': fs.createReadStream(signatureConfig.DETACHED_SIGNATURE_PATH),
        'signatures[0][documentContent]': fs.createReadStream(signatureConfig.ORIGINAL_DOCUMENT_PATH)
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL_SERVER_CMS, formData: verificationForm, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}

const verifyCAdESdetachedSignatureSendingOriginalDocumentHash = (URL_SERVER_CMS) => {

    const verificationForm = {
        'nonce': signatureConfig.NONCE,
        'signatures[0][nonce]': signatureConfig.NONCE_OF_SIGNATURE,
        'signatures[0][content]': fs.createReadStream(signatureConfig.DETACHED_SIGNATURE_PATH),
        'signatures[0][documentHashes][0]': signatureConfig.HASH_OF_ORIGINAL_DOCUMENT
    }

    return new Promise((resolve, reject) => {

        request.post({ url: URL_SERVER_CMS, formData: verificationForm, headers: this.headers }, (error, response, body) => {
            if (error) {
                reject(error);
            } else {
                resolve(JSON.parse(body));
            }
        })
    })

}

function sleep(millis) {
    return new Promise(resolve => setTimeout(resolve, millis));
}

module.exports = {
    URL_SERVER_CMS,
    verifyCAdESattachedSignature,
    verifyCAdESdetachedSignature,
    verifyCAdESdetachedSignatureSendingOriginalDocumentHash,
    configuredToken,
    sleep
};