module.exports = {

    // Request identifier
    NONCE: 1,

    // Identifier of the signature within a batch
    NONCE_OF_SIGNATURE: 1,

    // location where the attached signature is stored
    ATTACHED_SIGNATURE_PATH: './assinaturas/attached/AssinaturaAttachedADRC.p7s',

    // location where the detached signature is stored
    DETACHED_SIGNATURE_PATH: './assinaturas/detached/assinaturaDetached.p7s',

    // location where the original document is stored
    ORIGINAL_DOCUMENT_PATH: './assinaturas/detached/exemploTagAccept.pdf',

    // value of orignal document hash 
    HASH_OF_ORIGINAL_DOCUMENT: '05E6F73A05385C4A9E96B15EF3C75A75DE91632EE7DEB63BE098BC61072898FB'


}